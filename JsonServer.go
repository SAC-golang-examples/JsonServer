package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)


func main() {
	fmt.Println("[main] Se inicia en funcion main")
	puerto:="8080"
	fmt.Println("[main] Escuchando en el puerto: ["+puerto+"]");
	routerMux := mux.NewRouter().StrictSlash(true)
	routerMux.HandleFunc("/", Index)
	fmt.Println("[main] Escuchando en /");
	routerMux.HandleFunc("/ZWC/{operacion}", routerZWC).Queries("msisdn", "{msisdn}")
	fmt.Println("[main] Estan atendiendo los siguientes servicios:");
	for direccion := range alias{
		fmt.Println("[main]	/ZWC/"+direccion)
	}
	log.Fatal(http.ListenAndServe(":"+puerto, routerMux))
}
