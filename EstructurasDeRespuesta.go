package main

type ObtenerTipoProveedor struct {
	DESCRIPCION string `json:"DESCRIPCION"`
	STATUS      string `json:"STATUS"`
}

type ConsultarTipoClienteAmdocs struct {
	TipoSuscriptor int    `json:"tipoSuscriptor"`
	IDSuscriptor   string `json:"idSuscriptor"`
	CycleCode      int    `json:"cycleCode"`
} 

type ConsultarSaldoPrepago struct {
	BalanceAmount        float64 `json:"balanceAmount"`
	Code                 string  `json:"code"`
	PrepaidBalanceStatus string  `json:"prepaidBalanceStatus"`
}

type ObtenerSaldosAllowance struct {
	SaldoVoz       float64 `json:"saldoVoz"`
	SaldoMonetario float64 `json:"saldoMonetario"`
}

type EnviarMensajeSMS struct {
	Result string `json:"Result"`
}

type ResponderPeticionSoap struct {
}

type GuardarCodigoDeSalida struct {
	Result string `json:"Result"`
}

type GuardarHistoricoDeNotificaciones struct {
	Result string `json:"Result"`
}




/*
var happyPath MESSAGES = MESSAGES{
	PinchazoAmdocs: main.{
		ObtenerTipoProveedor: {
			DESCRIPCION: "AMD",
			STATUS:      "OK",
		},
		ConsultarTipoClienteAmdocs: {
			tipoSuscriptor: 0,
			idSuscriptor:   "IDSUSCRIPTOR",
			cycleCode:      1,
		},
		SaldoPrepagoDelCliente: {
			balanceAmount:        0.0,
			code:                 "0",
			prepaidBalanceStatus: "whatever",
		},
		ObtenerSaldosAllowance: {
			saldoVoz:       0.0,
			saldoMonetario: 5000.0,
		},
		EnviarMensajeSMS: {
			"Result": "Whatever",
		},
		ResponderPeticipnSoap: {},
		GuardarCodigoDeSalida: {
			Result: "Whatever",
		},
		GuardarHistoricoDeNotificaciones: {
			Result: "whatever",
		},
	},
}
*/
/*
{
	"CasoHappyPathAmdocs": {
		"ObtenerTipoProveedor": {
			"DESCRIPCION": "AMD",
			"STATUS": "OK"
		},
		"ConsultarTipoClienteAmdocs": {
			"tipoSuscriptor": 0,
			"idSuscriptor": "IDSUSCRIPTOR",
			"cycleCode": 1
		},
		"SaldoPrepagoDelCliente": {
			"balanceAmount": 0.0,
			"code": "0",
			"prepaidBalanceStatus": "whatever"
		},
		"ObtenerSaldosAllowance": {
			"saldoVoz": 0.0,
			"saldoMonetario": 5000.0
		},
		"EnviarMensajeSMS": {
			"Result": "Whatever"
		},
		"ResponderPeticipnSoap": {},
		"GuardarCodigoDeSalida": {
			"Result": "Whatever"
		},
		"GuardarHistoricoDeNotificaciones": {
			"Result": "whatever"
		}
	}
}

*/
