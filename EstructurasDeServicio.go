package main

type Pinchazo struct {
	ObtenerTipoProveedor `json:"ObtenerTipoProveedor"`
	ConsultarTipoClienteAmdocs `json:"ConsultarTipoClienteAmdocs"`
	ConsultarSaldoPrepago `json:"ConsultarSaldoPrepago"`
	ObtenerSaldosAllowance `json:"ObtenerSaldosAllowance"`
	EnviarMensajeSMS `json:"EnviarMensajeSMS"`
	ResponderPeticionSoap `json:"ResponderPeticipnSoap"`
	GuardarCodigoDeSalida `json:"GuardarCodigoDeSalida"`
	GuardarHistoricoDeNotificaciones `json:"GuardarHistoricoDeNotificaciones"`
}
