package main
import (
	"fmt"
	"net/http"
	"encoding/json"
	
	"github.com/gorilla/mux"
)

var ping = func(w http.ResponseWriter, r *http.Request) {
	fmt.Println("[ping]")
	w.Write([]byte("pong"))
}

var consultarHappyPath = func(w http.ResponseWriter, r *http.Request) {
	fmt.Println("[consultarHappyPath]")
	vars := mux.Vars(r)
	msisdn := vars["msisdn"]
	fmt.Println("[consultarHappyPath]:", happyPathPinchazo, msisdn)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(happyPathPinchazo); err != nil {
		panic(err)
	}
}

var consultarSaldoPrepago = func(w http.ResponseWriter, r *http.Request) {
	fmt.Println("[consultarSaldoPrepago]")
	vars := mux.Vars(r)
	msisdn := vars["msisdn"]
	fmt.Println("[consultarSaldoPrepago] Entradas: ", happyPathPinchazo, msisdn)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	var caso, ok = CasosPinchazo[msisdn]
	if !ok {
		caso = defaultPinchazo
	}
		var respuesta = caso.ConsultarSaldoPrepago
	if err := json.NewEncoder(w).Encode(respuesta); err != nil {
		panic(err)
	}
}

var def = func(w http.ResponseWriter, r *http.Request) {
	fmt.Println("[def]")
	w.Write([]byte("pong"))
}
var ConsultarDatosClienteAmdocs = func(w http.ResponseWriter, r *http.Request) {
	fmt.Println("[ConsultarDatosClienteAmdocs]")
	fmt.Println("[ConsultarDatosClienteAmdocs]",r.PostForm)
	for key, value := range r.PostForm {
		fmt.Println("[ConsultarDatosClienteAmdocs] Key:", key, "Value:", value)
	}
	w.Write([]byte(`<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:typ="http://telefonica.cl/CustomerManagement/CustomerInformationManagement/CustomerSubscriptionManagement/Coliving/V1/types" xmlns:v1="http://telefonica.cl/EnterpriseApplicationIntegration/TEFHeader/V1">
   <soapenv:Header/>
   <soapenv:Body>
	  <typ:GetColivingStatusByNumberResponse>
		 <typ:header>
			<!--Optional:-->
			<!--type: string-->
			<v1:idMessage>gero et</v1:idMessage>
			<!--type: string-->
			<v1:serviceName>sonoras imperio</v1:serviceName>
			<!--type: dateTime-->
			<v1:responseDateTime>2004-02-14T15:44:14</v1:responseDateTime>
			<!--type: string-->
			<v1:transactionID>temperat iras</v1:transactionID>
		 </typ:header>
		 <typ:data>
			<!--anonymous type-->
			<typ:status>t</typ:status>
			<!--anonymous type-->
			<typ:description>regemque dedit</typ:description>
		 </typ:data>
	  </typ:GetColivingStatusByNumberResponse>
   </soapenv:Body>
</soapenv:Envelope>`))
}

var opciones =func(w http.ResponseWriter, r *http.Request) {
	fmt.Println("[opciones]")
	host := r.URL.Host
	//"http://localhost:8080/"
	w.Write([]byte(`
   <p><a href="` + host + `/ConsultarDatosClienteAmdocs">ConsultarDatosClienteAmdocs</a> 
   <p><a href="` + host + `/ConsultarSaldoAllowance">ConsultarSaldoAllowance</a>
   <p><a href="` + host + `/ConsultarSaldoAmdocs">ConsultarSaldoAmdocs</a>
   <p><a href="` + host + `/ConsultarSaldoLegacy">ConsultarSaldoLegacy</a>
   <p><a href="` + host + `/ConsultarSaldoPrepago">ConsultarSaldoPrepago</a>
   <p><a href="` + host + `/ConsultarTipoClienteLegacy">ConsultarTipoClienteLegacy</a>
   <p><a href="` + host + `/ConsultarTipoClienteLegacy">ConsultarTipoClienteLegacy</a>
   <p><a href="` + host + `/ConsultarTipoClienteYSaldo">ConsultarTipoClienteYSaldo</a>
   <p><a href="` + host + `/ObtenerOrigenCliente">ObtenerOrigenCliente</a></p>
  `))
}

var Index = func(w http.ResponseWriter, r *http.Request) {
	fmt.Println("[Index]")
	fmt.Fprintln(w, "Welcome!")
}

var TodoIndex = func(w http.ResponseWriter, r *http.Request) {
	fmt.Println("[TodoIndex]")
	fmt.Fprintln(w, "Todo Index!")
}

var TodoShowUnElemento = func(w http.ResponseWriter, r *http.Request) {
	fmt.Println("[TodoShowUnElemento]")
	vars := mux.Vars(r)
	todoId := vars["todoId"]
	fmt.Fprintln(w, "Todo show:", todoId)
}

var TodoShow = func(w http.ResponseWriter, r *http.Request) {
	fmt.Println("[TodoShow]")
	vars := mux.Vars(r)
	todoId := vars["todoId"]
	msisdn := vars["msisdn"]
	fmt.Fprintln(w, "Todo show:", todoId, msisdn)
}

var NoExiste = func(w http.ResponseWriter, r *http.Request) {
	fmt.Println("[NoExiste]")
	fmt.Fprintln(w, "Lo sentimos, la pagina que buscas no existe")
}



