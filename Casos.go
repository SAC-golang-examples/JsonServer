package main

var CasosPinchazo=map[string]Pinchazo{
	"56910000000" : happyPathPinchazo,
}
/*
var Instancias = map[string]map[string]interface{
	"ObtenerTipoProveedor" : map[string]ObtenerTipoProveedor{
		"ok" : ObtenerTipoProveedor{DESCRIPCION: "AMD", STATUS: "OK"},
		"ok-amd" : ObtenerTipoProveedor{DESCRIPCION: "AMD", STATUS: "OK"},
		"nok-amd" : ObtenerTipoProveedor{DESCRIPCION: "AMD", STATUS: "NOK"},
		"ok-leg" : ObtenerTipoProveedor{DESCRIPCION: "LEG", STATUS: "OK"},
		"nok-leg" : ObtenerTipoProveedor{DESCRIPCION: "LEG", STATUS: "NOK"},
		"ok-desconocido" : ObtenerTipoProveedor{DESCRIPCION: "UNKNOWN", STATUS: "OK"}
	},
	"ConsultarSaldoPrepago" : map[string]ConsultarSaldoPrepago{
		"ok" : ConsultarSaldoPrepago{BalanceAmount: 0.0, Code: "0", PrepaidBalanceStatus: "whatever"},
		"ok-1000" : ConsultarSaldoPrepago{BalanceAmount: 1000.0, Code: "0", PrepaidBalanceStatus: "0"},
		"ok-5000" : ConsultarSaldoPrepago{BalanceAmount: 5000, Code: "0", PrepaidBalanceStatus: "0"},
		"ok-sinSaldo" : ConsultarSaldoPrepago{BalanceAmount: 0, Code: "0", PrepaidBalanceStatus: "0"},
		"nok-conSaldo" : ConsultarSaldoPrepago{BalanceAmount: 1000, Code: "-1", PrepaidBalanceStatus: "0"},
		"nok-sinSaldo" : ConsultarSaldoPrepago{BalanceAmount: 1000, Code: "-1", PrepaidBalanceStatus: "0"},
		"ok-saldo-status1000" : ConsultarSaldoPrepago{BalanceAmount : 1000, Code: "0", PrepaidBalanceStatus: "1000"},
	},
}
*/
var happyPathPinchazo Pinchazo = Pinchazo{
	ObtenerTipoProveedor:	Instancias_ObtenerTipoProveedor["ok-amd"],
	ConsultarTipoClienteAmdocs: Instancias_ConsultarTipoClienteAmdocs["ok"],
	ConsultarSaldoPrepago:	Instancias_ConsultarSaldoPrepago["ok-sinSaldo"],
	ObtenerSaldosAllowance: Instancias_ObtenerSaldosAllowance["ok"],
	EnviarMensajeSMS: Instancias_EnviarMensajeSMS["ok"],
	ResponderPeticionSoap: Instancias_ResponderPeticionSoap["ok"],
	GuardarCodigoDeSalida: Instancias_GuardarCodigoDeSalida["ok"],
	GuardarHistoricoDeNotificaciones: Instancias_GuardarHistoricoDeNotificaciones["ok"],
	
}


var defaultPinchazo Pinchazo = Pinchazo {
	ObtenerTipoProveedor:	Instancias_ObtenerTipoProveedor["ok"],
	ConsultarTipoClienteAmdocs: Instancias_ConsultarTipoClienteAmdocs["ok"],
	ConsultarSaldoPrepago:	Instancias_ConsultarSaldoPrepago["ok"],
	ObtenerSaldosAllowance: Instancias_ObtenerSaldosAllowance["ok"],
	EnviarMensajeSMS: Instancias_EnviarMensajeSMS["ok"],
	ResponderPeticionSoap: Instancias_ResponderPeticionSoap["ok"],
	GuardarCodigoDeSalida: Instancias_GuardarCodigoDeSalida["ok"],
	GuardarHistoricoDeNotificaciones: Instancias_GuardarHistoricoDeNotificaciones["ok"],
}

