package main

import (
	
	"fmt"
	"net/http"
)

func ping(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("pong"))
}
func def(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("pong"))
}

func ConsultarDatosClienteAmdocs(w http.ResponseWriter, r *http.Request) {
	fmt.Print(r.PostForm)
	/*soapAction := r.Header.Get("soapAction")
	v := SOAPEnvelope{
		Body: SOAPBody{
			Content: res,
		},
	}
	fmt.Println(soapAction)*/
	for key, value := range r.PostForm {
		fmt.Println("Key:", key, "Value:", value)
	}
	w.Write([]byte(`<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:typ="http://telefonica.cl/CustomerManagement/CustomerInformationManagement/CustomerSubscriptionManagement/Coliving/V1/types" xmlns:v1="http://telefonica.cl/EnterpriseApplicationIntegration/TEFHeader/V1">
   <soapenv:Header/>
   <soapenv:Body>
      <typ:GetColivingStatusByNumberResponse>
         <typ:header>
            <!--Optional:-->
            <!--type: string-->
            <v1:idMessage>gero et</v1:idMessage>
            <!--type: string-->
            <v1:serviceName>sonoras imperio</v1:serviceName>
            <!--type: dateTime-->
            <v1:responseDateTime>2004-02-14T15:44:14</v1:responseDateTime>
            <!--type: string-->
            <v1:transactionID>temperat iras</v1:transactionID>
         </typ:header>
         <typ:data>
            <!--anonymous type-->
            <typ:status>t</typ:status>
            <!--anonymous type-->
            <typ:description>regemque dedit</typ:description>
         </typ:data>
      </typ:GetColivingStatusByNumberResponse>
   </soapenv:Body>
</soapenv:Envelope>`))
}

func opciones(w http.ResponseWriter, r *http.Request) {
	host := r.URL.Host
	//"http://localhost:8080/"
	w.Write([]byte(`
   <p><a href="` + host + `/ConsultarDatosClienteAmdocs">ConsultarDatosClienteAmdocs</a> 
   <p><a href="` + host + `/ConsultarSaldoAllowance">ConsultarSaldoAllowance</a>
   <p><a href="` + host + `/ConsultarSaldoAmdocs">ConsultarSaldoAmdocs</a>
   <p><a href="` + host + `/ConsultarSaldoLegacy">ConsultarSaldoLegacy</a>
   <p><a href="` + host + `/ConsultarSaldoPrepago">ConsultarSaldoPrepago</a>
   <p><a href="` + host + `/ConsultarTipoClienteLegacy">ConsultarTipoClienteLegacy</a>
   <p><a href="` + host + `/ConsultarTipoClienteLegacy">ConsultarTipoClienteLegacy</a>
   <p><a href="` + host + `/ConsultarTipoClienteYSaldo">ConsultarTipoClienteYSaldo</a>
   <p><a href="` + host + `/ObtenerOrigenCliente">ObtenerOrigenCliente</a></p>
  `))
}

func main() {
	http.HandleFunc("/ConsultarDatosClienteAmdocs", ConsultarDatosClienteAmdocs)
	http.HandleFunc("/ConsultarSaldoAllowance", def)
	http.HandleFunc("/ConsultarSaldoAmdocs", def)
	http.HandleFunc("/ConsultarSaldoLegacy", def)
	http.HandleFunc("/ConsultarSaldoPrepago", def)
	http.HandleFunc("/ConsultarTipoClienteLegacy", def)
	http.HandleFunc("/ConsultarTipoClienteYSaldo", def)
	http.HandleFunc("/ObtenerOrigenCliente", def)
	http.HandleFunc("/", opciones)
	//  http.Handle("/", http.FileServer(http.Dir("./src")))
	http.HandleFunc("/ping", ping)
	if err := http.ListenAndServe(":8080", nil); err != nil {
		panic(err)
	}
}
