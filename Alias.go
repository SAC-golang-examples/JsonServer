package main
import (
	"net/http"
)

var alias = map[string]func(http.ResponseWriter, *http.Request){
	"Inicio" : Index,
	"ConsultarSaldoPrepago" : consultarSaldoPrepago,
	"ConsultarDatosClienteAmdocs" : ConsultarDatosClienteAmdocs,
	"opciones" : opciones,
	"happyPinchazo" : consultarHappyPath,
	"MensajePorDefecto" : NoExiste,
}
