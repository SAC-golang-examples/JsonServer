package main

//Instancia["ObtenerTipoProveedor"]["ok"];


var Instancias_ObtenerTipoProveedor = map[string]ObtenerTipoProveedor{
	"ok" : ObtenerTipoProveedor{DESCRIPCION: "AMD", STATUS: "OK"},
	"ok-amd" : ObtenerTipoProveedor{DESCRIPCION: "AMD", STATUS: "OK"},
	"nok-amd" : ObtenerTipoProveedor{DESCRIPCION: "AMD", STATUS: "NOK"},
	"ok-leg" : ObtenerTipoProveedor{DESCRIPCION: "LEG", STATUS: "OK"},
	"nok-leg" : ObtenerTipoProveedor{DESCRIPCION: "LEG", STATUS: "NOK"},
	"ok-desconocido" : ObtenerTipoProveedor{DESCRIPCION: "UNKNOWN", STATUS: "OK"},
}

var Instancias_ConsultarSaldoPrepago = map[string]ConsultarSaldoPrepago{
	"ok" : ConsultarSaldoPrepago{BalanceAmount: 0.0, Code: "0", PrepaidBalanceStatus: "whatever"},
	"ok-1000" : ConsultarSaldoPrepago{BalanceAmount: 1000.0, Code: "0", PrepaidBalanceStatus: "0"},
	"ok-5000" : ConsultarSaldoPrepago{BalanceAmount: 5000, Code: "0", PrepaidBalanceStatus: "0"},
	"ok-sinSaldo" : ConsultarSaldoPrepago{BalanceAmount: 0, Code: "0", PrepaidBalanceStatus: "0"},
	"nok-conSaldo" : ConsultarSaldoPrepago{BalanceAmount: 1000, Code: "-1", PrepaidBalanceStatus: "0"},
	"nok-sinSaldo" : ConsultarSaldoPrepago{BalanceAmount: 1000, Code: "-1", PrepaidBalanceStatus: "0"},
	"ok-saldo-status1000" : ConsultarSaldoPrepago{BalanceAmount : 1000, Code: "0", PrepaidBalanceStatus: "1000"},
}

var Instancias_ConsultarTipoClienteAmdocs = map[string]ConsultarTipoClienteAmdocs{
	"ok" : ConsultarTipoClienteAmdocs{TipoSuscriptor: 0, IDSuscriptor: "IDSUSCRIPTOR-01", CycleCode: 1},
	"prepago": ConsultarTipoClienteAmdocs{TipoSuscriptor: 0, IDSuscriptor: "IDSUSCRIPTOR-01", CycleCode: 1},
	"postpago": ConsultarTipoClienteAmdocs{TipoSuscriptor: 1, IDSuscriptor: "IDSUSCRIPTOR-01", CycleCode: 1},
	"control": ConsultarTipoClienteAmdocs{TipoSuscriptor: 2, IDSuscriptor: "IDSUSCRIPTOR-01", CycleCode: 1},
	"hibrido": ConsultarTipoClienteAmdocs{TipoSuscriptor: 3, IDSuscriptor: "IDSUSCRIPTOR-01", CycleCode: 1},
	"nok" : ConsultarTipoClienteAmdocs{TipoSuscriptor: 4, IDSuscriptor: "IDSUSCRIPTOR-01", CycleCode: 1},
}

var Instancias_ObtenerSaldosAllowance = map[string]ObtenerSaldosAllowance{
	"ok" : ObtenerSaldosAllowance{SaldoVoz: 0.0, SaldoMonetario: 5000.0},
	"sinSaldo" : ObtenerSaldosAllowance{SaldoVoz: 0.0, SaldoMonetario: 0.0},
}


var Instancias_EnviarMensajeSMS = map[string]EnviarMensajeSMS{
	"ok" : EnviarMensajeSMS{Result: "whatever"},
	"nok" : EnviarMensajeSMS{Result: "whateverNOK"},
}

var Instancias_ResponderPeticionSoap = map[string]ResponderPeticionSoap{
	"ok" : ResponderPeticionSoap{},
	"nok" : ResponderPeticionSoap{},
}

var Instancias_GuardarCodigoDeSalida = map[string]GuardarCodigoDeSalida{
	"ok" : GuardarCodigoDeSalida{Result: "whatever"},
	"nok" : GuardarCodigoDeSalida{Result: "whateverNOK"},
}

var Instancias_GuardarHistoricoDeNotificaciones = map[string]GuardarHistoricoDeNotificaciones{
	"ok" : GuardarHistoricoDeNotificaciones{Result: "whatever"},
	"nok" : GuardarHistoricoDeNotificaciones{Result: "whateverNOK"},
}
