package main
import (
	"fmt"
	"net/http"
	
	"github.com/gorilla/mux"
)

func routerZWC(w http.ResponseWriter, r *http.Request) {
	fmt.Println("[router]")
	vars := mux.Vars(r)
	fmt.Println("[router] Se tienen las siguientes variables de entrada:", vars)
	operacion:=vars["operacion"]
	fmt.Println("[router] Se intenta llamar al siguiente alias:",operacion)
	var funcion, ok = alias[operacion]
	if ok{
		fmt.Println("[router] Alias existe, llamando a operacion ", operacion )
		funcion(w,r)
	}else{
		fmt.Println("[router] No existe operacion relacionada. Ejecutando operacion por defecto ", operacion )
		alias["MensajePorDefecto"](w,r)
	}
}

